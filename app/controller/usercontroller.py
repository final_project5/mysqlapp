from flask.helpers import safe_join
from app.models.usermodels import Database_user
from app.models.usermodels import *
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqlcrud = Database_user()

def shows():
    dbresult = mysqlcrud.showUser()
    result = []
    for items in dbresult:
        user = {
            "iduser" : items.iduser,
            "email" : items.email,
            "password" : items.password,
            "nama_lengkap" : items.nama_lengkap,
            "gender" : items.gender,
            "no_telp" : items.no_telp,
        }
        result.append(user)
    return jsonify(result)

def token(**params):
    dbresult = mysqlcrud.showUserByEmail(**params)
    if dbresult is not None:
        for items in dbresult:
            user = {
                "email" : items.email,
                "nama_lengkap" : items.nama_lengkap,
            }
            expires = datetime.timedelta(days = 1)
            access_token = create_access_token(user, fresh=True, expires_delta=expires)

            data = {
                "data": user,
                "token_access": access_token
            }
    else:
        data = {
            "message": "Hasil tidak terdaftar"
        }
    return jsonify(data)

def insert(**param):
    mysqlcrud.insertUser(**param)
    data = {
        "message":"Berhasil di masukkan"
    }
    return jsonify(data)

def showid(param):
    dbresult = mysqlcrud.showUserById(param)
    if dbresult is not None:
        for items in dbresult:
            user = {
                "email" : items.email,
                "password" : items.password,
                "nama_lengkap" : items.nama_lengkap,
                "gender" : items.gender,
                "no_telp" : items.no_telp

            }
            expires = datetime.timedelta(days = 1)
            access_token = create_access_token(user, fresh=True, expires_delta=expires)

            data = {
                "data": user,
                "token_access": access_token
            }
    else:
        data = {
            "message": "Hasil tidak terdaftar"
        }
    return jsonify(data)

def delete(param):
    mysqlcrud.deleteUserById(param)
    data = {
        "message":"data berhasil dihapus"
    }
    return jsonify(data)

def update(param):
    mysqlcrud.updateUserById(param)
    data={
        "message":"Data Berhasil Di Update"
    }
    return jsonify(data)
