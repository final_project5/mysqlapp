from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.userrouter import *
app.register_blueprint(user_blueprint)

from app.routers.produkrouter import *
app.register_blueprint(produk_blueprint)

from app.routers.transaksirouter import *
app.register_blueprint(transaksi_blueprint)