from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy import *
from sqlalchemy.orm import backref

 
#connector ke database
Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Asaulter123@localhost:3306/final_project', echo=True)
Session = sessionmaker(bind=engine)
session = Session()


#class untuk table
class Produk(Base):
    __tablename__ = 'produk'
    idproduk = Column(Integer, primary_key =  True, autoincrement= True, nullable = False )
    nama = Column(String)
    price = Column(String)
    nama_toko = Column(String)
    stock = Column(Integer)

#crud data produk
class Database_produk():
    def showProduk(self):
        try:
            result = session.query(Produk).all()
            return result
        except Exception as e:
            print(e)
    def insertProduk(self, **param):
        try:
            session.add(Produk(**param))
            session.commit()
        except Exception as e:
            print(e)
    def updateProduk(self, data):
        try:
            result = session.query(Produk).filter(Produk.idproduk == data['idproduk']).one()
            result.nama = data['nama']
            result.price = data['price']
            result.nama_toko = data['nama_toko']
            result.stock = data['stock']
            session.commit()
        except Exception as e:
            print(e)
    def deleteProduk(self, data):
        try:
            result = session.query(Produk).filter(Produk.idproduk == data).one()
            session.delete(result)
            session.commit()
        except Exception as e:
            print(e)
    def showProdukByToko(self, data):
        try:
            result = session.query(Produk).filter(Produk.nama_toko == data)
            return result
        except Exception as e:
            print(e)