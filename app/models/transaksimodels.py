from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy import *
from sqlalchemy.orm import backref

#connector ke database
Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Asaulter123@localhost:3306/final_project', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

class Produk(Base):
    __tablename__ = 'produk'
    idproduk = Column(Integer, primary_key =  True, autoincrement= True, nullable = False )
    nama = Column(String)
    price = Column(String)
    nama_toko = Column(String)
    stock = Column(Integer)

class User(Base):
    __tablename__ = 'user'
    iduser = Column(Integer, primary_key =  True, autoincrement= True, nullable = False )
    email = Column(String, nullable = False)
    password = Column(String, nullable = False)
    nama_lengkap = Column(String)
    gender = Column(String)
    no_telp = Column(String)

class Transaksi(Base):
    __tablename__ = 'transaksi'
    idtransaksi = Column(Integer, primary_key =  True, autoincrement= True, nullable = False)
    total = Column(Integer)
    iduser = Column(Integer, ForeignKey('user.iduser'))
    idproduk = Column(Integer, ForeignKey('produk.idproduk'))

class Database_transaksi():
    def showTransaksiByUserId(self, param):
        try:
            result = session.query(Transaksi).filter(Transaksi.iduser == param)
            return result
        except Exception as e:
            print(e)
    def insertTransaksi(self, **param):
        try:
            session.add(Transaksi(**param))
            session.commit()
        except Exception as e:
            print(e)
    def deleteTransaksi(self, param):
        try:
            result = session.query(Transaksi).filter(Transaksi.idtransaksi == param).one()
            session.delete(result)
            session.commit()
        except Exception as e:
            print(e)
    def showalltransaksi(self):
        try:
            result = session.query(Transaksi).all()
            return result
        except Exception as e:
            print(e)