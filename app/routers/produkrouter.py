from sqlalchemy.sql.functions import user
from app import app
from app.controller import produkcontroller
from app.controller.produkcontroller import *
from flask import Blueprint, request

#produk blueprint
produk_blueprint = Blueprint("produkcontroller", __name__)

#route untuk menampilkan semua produk
@app.route("/produk", methods = ["GET", "POST"])
def lihatsmua():
    return produkcontroller.tampilall()

#route untuk insert data produk
@app.route("/produk/insert", methods = ["GET", "POST"] )
def masukkandata():
    param = request.json
    return produkcontroller.masukproduk(**param)

#toko untuk update data produk
@app.route("/produk/update", methods = ["GET", "POST"])
def updatedata():
    param = request.json
    return produkcontroller.updateproduk(param)

#toko untuk delet data produk
@app.route("/produk/delete", methods = ["GET", "POST"])
def deletedata():
    param = request.json
    return produkcontroller.hapusproduk(param['idproduk'])

#toko untuk menampilkan data produk berdasarkan nama toko
@app.route("/produk/nama_toko", methods = ["GET", "POST"])
def showpertoko():
    param = request.json
    return produkcontroller.produkpertoko(param['nama_toko'])