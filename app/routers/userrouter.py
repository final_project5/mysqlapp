from sqlalchemy.sql.functions import user
from app import app
from app.controller import usercontroller
from app.controller.usercontroller import *
from flask import Blueprint, request

user_blueprint = Blueprint("userrouter", __name__)

@app.route("/user", methods=["GET"])
def showUser():
    return usercontroller.shows()

@app.route("/user/requesttoken", methods=["GET", "POST"])
def requestToken():
    param = request.json
    return usercontroller.token(**param)

@app.route("/user/insert", methods=["GET", "POST"])
def insertdata():
    param = request.json
    return usercontroller.insert(**param)

@app.route("/user/byid", methods = ["GET", "POST"])
def perid():
    param = request.json
    idd = param['iduser']
    return usercontroller.showid(idd)

@app.route("/user/delete", methods = ["GET", "POST"])
def hapus():
    param = request.json
    idd = param['iduser']
    return usercontroller.delete(idd)

@app.route("/user/update", methods = ["GET", "POST"])
def ubah():
    param = request.json
    return usercontroller.update(param)