from sqlalchemy.sql.functions import user
from app import app
from app.controller import transaksicontroller
from app.controller.transaksicontroller import *
from flask import Blueprint, request

#produk blueprint
transaksi_blueprint = Blueprint("transaksicontroller", __name__)

@app.route("/transaksi/insert", methods=["GET", "POST"])
def beli():
    param = request.json
    return transaksicontroller.addTransaksi(**param)

@app.route("/transaksi/byuserid", methods = ["GET", "POST"])
def show():
    param = request.json
    return transaksicontroller.tampilkantransaksibyid(param['iduser'])

@app.route("/transaksi/delete", methods =["GET", "POST"])
def hapustra():
    param = request.json
    return transaksicontroller.hapustransaksi(param['idtransaksi'])

@app.route("/transaksi", methods =["GET", "POST"])
def all():
    return transaksicontroller.tampilkansmuatransaksi()