# MYSQLAPP

## SIMPLE BACKEND MICROSERVICE ONLINE SHOP DENGAN MYSQL

---

Aplikasi ini adalah backend microservice simple menggunakan FLASK, MYSQL, dengan ORM sebagai penulisan kodenya. beberapa fungsi menggunakan JWT untuk keamanan, sebagai contoh pada fungsi untuk menampilkan data user berdasarkan id dan juga menampilkan data user berdasarkan email.

---
## Langkah penggunaan
Fungsi yang ada pada aplikasi ini adalah :
1. user
    * Menampilkan seluruh data user dengan cara mengimputkan *** http://127.0.0.1:5000/user *** pada postman
    * Insert data user dengan cara *** http://127.0.0.1:5000/user/insert *** pada postman dan menuliskan data yang akan dimasukkan, sebagai contoh {"email" : "Hiko@gmail.com", "password" : "hiko123", "nama_lengkap" : "hiko 100T", "gender" : "Laki - Laki", "no_telp" : "0846546" }
    * Update data user dengan cara masuk pada ***  http://127.0.0.1:5000/user/update *** pada postman dan menuliskan data baru yang akan digunakan untuk mengubah data lama sebagai contoh { "iduser" : 12, "email" : "singsing@gmail.com", "password" : "sing123", "nama_lengkap" : "C9 Singsing", "gender" : "Laki - Laki", "no_telp" : "0846546798" }. Jangan lupa masukkan "iduser" pada data baru sebagai acuan data lama yang akan diubah
    * delete data user dengan cara masuk pada *** http://127.0.0.1:5000/user/delete *** pada postman dan tuliskan { "iduser" : 12 } iduser yang ingin di hapus
    * menampilkan data dan token JWT dengan cara masuk pada *** http://127.0.0.1:5000/user/byid *** berdasarkan id 
    * menampilkan data dan token JWT dengan cara masuk pada *** http://127.0.0.1:5000/user/requesttoken *** berdasarkan email

2. Produk
    * Menampilkan seluruh data produk dengan cara masuk pada *** http://127.0.0.1:5000/produk *** pada postman
    * Insert data produk dengan cara akses *** http://127.0.0.1:5000/produk/insert *** dan masukkan data yang ingin dimasukkan, sebagai contoh {"nama": "Laptop Lenovo", "price": "7500000", "nama_toko": "Lenovo Official Store", "stock": 20 }
    * Update data produk dengan cara akses *** http://127.0.0.1:5000/produk/update *** pada postman dan jangan lupa masukkan data baru dan idproduknya yang ingin diubah, sebagai contoh { "idproduk" : 7, "nama": "Laptop Hp", "price": "7550000", "nama_toko": "Hp Official Store", "stock": 22 }
    * Delete Data produk dengan cara akses *** http://127.0.0.1:5000/produk/delete *** pada postman dan masukkan idproduk yang akan dihapus
    * menampilkan data berdasarkan nama toko dengan cara akses *** http://127.0.0.1:5000/produk/nama_toko *** dan masukkan nama tokonya

3. Transaksi
    * Menampilkan seluruh data transaksi dengan cara masuk pada *** http://127.0.0.1:5000/transaksi ***
    * Menampilkan data transaksi berdasarkan iduser *** http://127.0.0.1:5000/transaksi/byuserid *** dan masukkan iduser
    * insert data transaksi baru *** http://127.0.0.1:5000/transaksi/insert *** masukkan total, iduser, dan idproduk
    * Menghapus data transaksi berdasarkan idtransaksi *** http://127.0.0.1:5000/transaksi/delete *** dan masukkan id transaksi


---
## ERD

* dapat di akses pada erd.png

## Biodata

* Dapat di akses pada biodata.txt